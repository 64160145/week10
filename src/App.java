public class App {
    public static void main(String[] args) {
        Rectangle rec = new Rectangle(5, 3);
        System.out.println(rec.toString());
        System.out.println(rec.calArea());
        System.out.println(rec.calPerimeter());

        Rectangle rec2 = new Rectangle(2, 2);
        System.out.println(rec2.toString());
        System.out.println(rec2.calArea());
        System.out.println(rec2.calPerimeter());

        Circle cir = new Circle(2);
        System.out.println(cir.toString());
        System.out.printf("%s area: %.3f \n", cir.getName(), cir.calArea());
        System.out.printf("%s perimeter: %.3f \n", cir.getName(), cir.calPerimeter());

        Circle cir2 = new Circle(3);
        System.out.println(cir2.toString());
        System.out.printf("%s area: %.3f \n", cir2.getName(),cir2.calArea());
        System.out.printf("%s perimeter: %.3f \n", cir2.getName(),cir2.calPerimeter());

        Triangle tri = new Triangle(5, 5, 6);
        System.out.println(tri.toString());
        System.out.printf("%s area: %.3f \n", tri.getName(),tri.calArea());
        System.out.printf("%s perimeter: %.3f \n", tri.getName(),tri.calPerimeter());

        Triangle tri2 = new Triangle(7, 5, 6);
        System.out.println(tri2.toString());
        System.out.printf("%s area: %.3f \n", tri2.getName(),tri2.calArea());
        System.out.printf("%s perimeter: %.3f \n", tri2.getName(),tri2.calPerimeter());
    }
}
